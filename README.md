# Symmetric Venn Diagram With Equal Regions

I proved [this Puzzling StackExchange question](https://puzzling.stackexchange.com/questions/89252/venn-diagram-with-7-equal-regions)
via Monte Carle sampling (as always...) and expanded to `N` circles.


I generated `N` circles with radius `r = 1` and radius `R` from the center of the ensemble. 
I also uniformly generated `n` random points over the ensemble.
An estimation of the regional area of was found by counting all points within each region.
By varying `R` an estimation of area per region as a function of `R` was achieved.
Below is an illustrative example with `N = 3`, `n = 10000`, and `R = 0.404`.

![](Results/venn_N=3_r=1_n=10000_R=0.404.png)


Below is the regional ("Partition" in the figure) area as a function of `R`. 
The area of one region monotonically decreases (6: the center region overlapped by all circles),
some regions monotonically increase (0, 1, 2: the outer regions with no overlap),
and some regions increase and then decrease (3, 4, 5: the regions overlapped by only one adjacent circle).
Symmetry allows for a "kind" of region, where the number of kinds is equal to the number of circles.
Because the exists no `R` where the area of each kind are equal, 
the solution to the linked StackExchange question above is a qualitative "No.".
![](Results/venn_N=3_r=1_n=10000_dR=0.05.png)

This may be extended to N circles, where only `N = {2, 4, 5}` are shown below.
Note that `R = 0.404` is the only value of `R` -- and only valid for `N = 2` -- 
where all regions have equal area.

<p float="left">
  <img src="Results/venn_N=2_r=1_n=10000_R=0.404.png" width="444" />
  <img src="Results/venn_N=2_r=1_n=10000_dR=0.05.png" width="444" />
</p>

<p float="left">
  <img src="Results/venn_N=4_r=1_n=10000_R=0.404.png" width="444" />
  <img src="Results/venn_N=4_r=1_n=10000_dR=0.05.png" width="444" />
</p>

<p float="left">
  <img src="Results/venn_N=5_r=1_n=10000_R=0.404.png" width="444" />
  <img src="Results/venn_N=5_r=1_n=10000_dR=0.05.png" width="444" />
</p>
