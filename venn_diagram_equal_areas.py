import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.patches as patches
from itertools import combinations


'''
N = 3 # number of circles
r = 1 # radius of circle
ORIGIN = (0,0) # shared origin of circles

# R here is the radius to solve for, where all valid values produce partitions of equal area.
# 0. < R < 1.
R = 0.404

n = 5000

origin = np.array(ORIGIN)
theta1 = 2*np.pi/N
thetas = np.arange(0, N*theta1, theta1)
centers = np.array([R * r * np.array([np.cos(i), np.sin(i)]) + origin for i in thetas])
centers = centers.reshape(-1, 2)

fig, ax = plt.subplots(figsize=(10,10))
plt.scatter(*centers.T, label='Circle center', s=10)
plt.scatter(*origin, label='Shared origin', s=10)
circles = []
for e,i in enumerate(centers):
    circle = plt.Circle(i, r, fill=False)
    #circle = patches.Circle(i, R, fill=False)
    circles.append(circle)
    ax.add_artist(circle)
   
    plt.text(i[0], i[1], str(e))

lims = origin + 2*np.array([[-r], [r]])
x = np.random.uniform(*lims[:,0], n)
y = np.random.uniform(*lims[:,1], n)
points = np.array(list(zip(x,y)))
points_valid = np.array([i for i in points if any([c.contains_point(ax.transData.transform(i)) for c in circles])])
plt.scatter(*points.T, s=1, c='pink')
plt.scatter(*points_valid.T, s=1, c='r', label='MC')

plt.axis('scaled')
#plt.xlim(*lims[:,0])
#plt.ylim(*lims[:,1])
plt.title('N = {}, r = {}; n = {}, R = {}'.format(N, r, n, R))
plt.grid()
plt.legend()
plt.savefig('Results/venn_N={}.png'.format(N))
plt.show(block=False)
'''

'''
combos = [list(i) for j in range(1, N+1) for i in combinations(range(N), j)]

points_valid = []
for p in points:
    x = [c.contains_point(ax.transData.transform(p)) for c in circles]
    y = [e for e,i in enumerate(x) if i]
    z = [e for e,i in enumerate(combos) if i==y]
    if z:
        points_valid.append(np.concatenate((p, z)))
points_valid = np.array(points_valid)

areas = [points_valid[points_valid[:,2]==i].shape[0] for i in range(len(combos))]
'''






### Do above for real.
def do(N=3, r=1, n=10000, dR=0.05, ORIGIN=(0,0)):

    R = 0.404

    origin = np.array(ORIGIN)
    theta1 = 2*np.pi/N
    thetas = np.arange(0, N*theta1, theta1)
    centers = np.array([R * r * np.array([np.cos(i), np.sin(i)]) + origin for i in thetas])
    centers = centers.reshape(-1, 2)

    fig, ax = plt.subplots(figsize=(10,10))
    plt.scatter(*centers.T, label='Circle center', s=10)
    plt.scatter(*origin, label='Shared origin', s=10)
    circles = []
    for e,i in enumerate(centers):
        circle = plt.Circle(i, r, fill=False)
        #circle = patches.Circle(i, R, fill=False)
        circles.append(circle)
        ax.add_artist(circle)
    
        plt.text(i[0], i[1], str(e))
    
    lims = origin + 2*np.array([[-r], [r]])
    x = np.random.uniform(*lims[:,0], n)
    y = np.random.uniform(*lims[:,1], n)
    points = np.array(list(zip(x,y)))
    points_valid = np.array([i for i in points if 
        any([c.contains_point(ax.transData.transform(i)) for c in circles])])
    plt.scatter(*points.T, s=1, c='pink')
    plt.scatter(*points_valid.T, s=1, c='r', label='MC')
    
    plt.axis('scaled')
    #plt.xlim(*lims[:,0])
    #plt.ylim(*lims[:,1])
    plt.title('N = {}, r = {}; n = {}, R = {}'.format(N, r, n, R))
    plt.grid()
    plt.legend()
    plt.savefig('Results/venn_N={}_r={}_n={}_R={}.png'.format(N, r, n, R))
    plt.show(block=False)





    AREAS = []
    R = []

    origin = np.array(ORIGIN)
    theta1 = 2*np.pi/N
    thetas = np.arange(0, N*theta1, theta1)

    lims = origin + 2*np.array([[-r], [r]])
    x = np.random.uniform(*lims[:,0], n)
    y = np.random.uniform(*lims[:,1], n)
    points = np.array(list(zip(x,y)))


 
    combos = [list(i) for j in range(1, N+1) for i in combinations(range(N), j)]
    
    fig, ax = plt.subplots()
    for R_ in np.arange(0, r+dR, dR):
        R.append(R_)
    
        centers = np.array([R_ * r * np.array([np.cos(i), np.sin(i)]) + origin for i in thetas])
        centers = centers.reshape(-1, 2)
    
        circles = []
        for e,i in enumerate(centers):
            circle = plt.Circle(i, r, fill=False)
            circles.append(circle)
            ax.add_artist(circle)
    
        points_valid = []
        for p in points:
            a = [i.contains_point(ax.transData.transform(p)) for i in circles]
            b = [e for e,i in enumerate(a) if i]
            c = [e for e,i in enumerate(combos) if i==b]
            if c:
                points_valid.append(np.concatenate((p, c)))
        points_valid = np.array(points_valid)

        areas = [points_valid[points_valid[:,2]==i].shape[0] for i in range(len(combos))]
        AREAS.append(areas)
    plt.savefig('Results/lines_N={}_r={}_n={}_dR={}.png'.format(N, r, n, dR))
    plt.show(block=False)

    AREAS = np.array(AREAS)
    R = np.array(R)


    plt.figure()
    for e,i in enumerate(AREAS.T):
        plt.plot(R, i, label='{}'.format(e))
    plt.title('N = {}, r = {}; n = {}, dR = {}'.format(N, r, n, dR))
    plt.xlabel('Radius')
    plt.ylabel('Area (au)')
    plt.legend(title='Partition')
    plt.savefig('Results/venn_N={}_r={}_n={}_dR={}.png'.format(N, r, n, dR))
    plt.show(block=False)

    return N, r, n, dR, AREAS, R


for i in range(2, 6):
    N, r, n, dR, AREAS, R = do(i)




# for i in points:
#     count = sum([j.contains_point(ax.transData.transform(i)) for j in circles])
#     print count,

# for e,c in enumerate(circles):
#     count = sum([c.contains_point(ax.transData.transform(p)) for p in points])
#     print(count)


# I should revisit https://gitlab.com/Bonatt/AvgDistInCircle/blob/master/AvgDistInCircle.py
# as reference to calc MC uncertainty
